﻿using AutoMapper;
using BlogWebApplication.Dtos;
using BlogWebApplication.Entities;

namespace BlogWebApplication
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            FromDataAccessorLayer();
            FromPresentationLayer();
        }

        private void FromPresentationLayer()
        {
            CreateMap<ProjectDto, Project>().ReverseMap();
        }

        private void FromDataAccessorLayer()
        {

        }
    }
}
