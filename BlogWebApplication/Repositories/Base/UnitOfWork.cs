﻿using BlogWebApplication.DbContexts;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace BlogWebApplication.Repositories.Base
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext _context = null;

        public UnitOfWork(DataContext context)
        {
            _context = context;
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
    }
}
