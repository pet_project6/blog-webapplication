﻿namespace BlogWebApplication.Repositories.Base
{
    public interface IUnitOfWork : IDisposable
    {
        int SaveChanges();
    }
}
