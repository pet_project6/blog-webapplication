﻿using System.Linq.Expressions;

namespace BlogWebApplication.Repositories.Base
{
    public interface IRepository<TEntity> where TEntity : class
    {
        void Delete(object id);
        void Delete(TEntity entity);
        TEntity Find(object id);
        void Insert(TEntity entity);
        void InsertRange(IEnumerable<TEntity> entities);
        IQueryable<TEntity> Queryable();
        IQueryable<TEntity> AsNoTracking();
        void Update(TEntity entity);
        void UpdateRange(IEnumerable<TEntity> entities);
    }
}
