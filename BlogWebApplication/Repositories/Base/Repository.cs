﻿using BlogWebApplication.DbContexts;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace BlogWebApplication.Repositories.Base
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly DataContext _context = null;
        private readonly DbSet<TEntity> _table = null;

        public Repository(DataContext context)
        {
            _context = context;
            _table = _context.Set<TEntity>();
        }

        public void Delete(object id)
        {
            TEntity entity = _table.Find(id);
            _table.Remove(entity);
        }

        public void Delete(TEntity entity)
        {
            if (_context.Entry(entity).State == EntityState.Detached)
            {
                _table.Attach(entity);
            }
            _table.Remove(entity);
        }

        public TEntity Find(object id)
        {
            return _table.Find(id);
        }

        public void Insert(TEntity entity)
        {
            _table.Add(entity);
        }

        public void InsertRange(IEnumerable<TEntity> entities)
        {
            _table.AddRange(entities);
        }

        public IQueryable<TEntity> Queryable()
        {
            return _table.AsQueryable();
        }

        public IQueryable<TEntity> AsNoTracking()
        {
            return _table.AsNoTracking();
        }

        public void Update(TEntity entity)
        {
            _table.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
        }

        public void UpdateRange(IEnumerable<TEntity> entities)
        {
            _table.UpdateRange(entities);
        }
    }
}
