﻿using AutoMapper;
using BlogWebApplication.DbContexts;
using BlogWebApplication.Entities;
using BlogWebApplication.Repositories.Base;
using BlogWebApplication.Services.Contract;
using BlogWebApplication.Services.Implementation;
using Microsoft.EntityFrameworkCore;

namespace BlogWebApplication
{
    public static class ServiceExtensions
    {
        public static void RegisterServices(this IServiceCollection services, IConfiguration configuration) 
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfile());
            });

            var mapper = config.CreateMapper();

            services.AddSingleton(mapper);

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddDbContext<DataContext>(options =>
                    options.UseSqlServer(configuration.GetConnectionString("Default")));
            services.AddScoped<IRepository<Project>, Repository<Project>>();
            services.AddScoped<IProjectService, ProjectService>();
        }
    }
}
