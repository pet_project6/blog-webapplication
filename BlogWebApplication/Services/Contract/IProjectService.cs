﻿using BlogWebApplication.Dtos;

namespace BlogWebApplication.Services.Contract
{
    public interface IProjectService
    {
        void AddProject(ProjectDto projectDto);
        void UpdateProject(ProjectDto projectDto);
        List<ProjectDto> GetProjects(int projectId);
    }
}
