﻿using AutoMapper;
using BlogWebApplication.Dtos;
using BlogWebApplication.Entities;
using BlogWebApplication.Repositories.Base;
using BlogWebApplication.Services.Contract;

namespace BlogWebApplication.Services.Implementation
{
    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Project> _projectRepository;
        private readonly IMapper _mapper;
        public ProjectService(
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IRepository<Project> projectRepository
            )
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _projectRepository = projectRepository;
        }

        public void AddProject(ProjectDto projectDto)
        {
            var project = _mapper.Map<Project>(projectDto);
            _projectRepository.Insert(project);
            _unitOfWork.SaveChanges();
        }

        public List<ProjectDto> GetProjects(int projectId)
        {
            throw new NotImplementedException();
        }

        public void UpdateProject(ProjectDto projectDto)
        {
            var project = _mapper.Map<Project>(projectDto);
            _projectRepository.Update(project);
            _unitOfWork.SaveChanges();
        }
    }
}
