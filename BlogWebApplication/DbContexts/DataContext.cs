﻿using BlogWebApplication.Entities;
using BlogWebApplication.EntityConfigurations;
using Microsoft.EntityFrameworkCore;

namespace BlogWebApplication.DbContexts
{
    public class DataContext : DbContext
    {
        protected readonly IConfiguration Configuration;
        public DataContext(DbContextOptions<DataContext> options, IConfiguration configuration)
            : base(options)
        {
            Configuration = configuration;
        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(Configuration.GetConnectionString("Default"));
        }

        //All database override will be apply here
        protected override void OnModelCreating(ModelBuilder builder)
        {
            #region Entity Configurations
            builder.ApplyConfiguration(new ProjectEntityTypeConfiguration());

            #endregion

            base.OnModelCreating(builder);


        }
        #region Entities
        public virtual DbSet<Project> Projects { get; set; }

        #endregion

    }
}
