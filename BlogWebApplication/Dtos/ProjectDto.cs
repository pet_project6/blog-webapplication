﻿namespace BlogWebApplication.Dtos
{
    public class ProjectDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
