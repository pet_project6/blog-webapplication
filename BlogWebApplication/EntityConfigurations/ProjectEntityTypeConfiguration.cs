﻿using BlogWebApplication.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BlogWebApplication.EntityConfigurations
{
    public class ProjectEntityTypeConfiguration
        : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> builder)
        {
            builder.ToTable("Projects");

            builder.HasKey(ci => ci.Id);

            #region User
            //builder.HasOne(ci => ci.CreatedByUser)
            //    .WithMany()
            //    .HasForeignKey(ci => ci.CreatedBy);
            //builder.HasOne(ci => ci.UpdatedByUser)
            //    .WithMany()
            //    .HasForeignKey(ci => ci.UpdatedBy);
            #endregion
        }
    }
}
