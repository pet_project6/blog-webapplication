﻿using BlogWebApplication.Dtos;
using BlogWebApplication.Models;
using BlogWebApplication.Services.Contract;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace BlogWebApplication.Controllers
{
    public class ProjectController : Controller
    {
        private readonly IProjectService _projectService;

        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        // GET: ProjectController
        public ActionResult Index()
        {
            var projects = _projectService.GetProjects(1);
            return View(projects);
        }

        // GET: ProjectController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProjectController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateAsync(ProjectDto project)
        {
            try
            {
                _projectService.AddProject(project);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

    }
}
